// header 1

const burgerHeader1 = document.getElementById('burger-header1')
const phoneMenuHeader1 = document.getElementById('phone-menu-header1');
let header1Switch = false;

burgerHeader1.addEventListener('click', () => {
    if (!header1Switch) {
        phoneMenuHeader1.style.display = 'flex';
        header1Switch = true;
    } else {
        phoneMenuHeader1.style.display = 'none';
        header1Switch = false;
    }
})


// header 2


const formHeader2 = document.querySelector<HTMLFormElement>('#form-header2')
const inputHeader2 = document.querySelector<HTMLInputElement>('#input-header2');

const formMenuHeader2 = document.querySelector<HTMLFormElement>('#form-menu-header2')
const inputMenuHeader2 = document.querySelector<HTMLInputElement>('#input-menu-header2');

const burgerHeader2 = document.getElementById('burger-header2')
const phoneMenuHeader2 = document.getElementById('phone-menu-header2');
let header2Switch = false;

burgerHeader2.addEventListener('click', () => {
    if (!header2Switch) {
        phoneMenuHeader2.style.display = 'flex';
        header2Switch = true;
    } else {
        phoneMenuHeader2.style.display = 'none';
        header2Switch = false;
    }
})


formHeader2.addEventListener('submit', () => {
    alert(inputHeader2.value)
    inputHeader2.value = ""
})

formMenuHeader2.addEventListener('submit', () => {
    alert(inputMenuHeader2.value)
    inputMenuHeader2.value = ""
})


// header 3

const MainOptions1Header3 = document.getElementById('OptionMain1-header3');
const MainOptions2Header3 = document.getElementById('OptionMain2-header3');
const MainOptions3Header3 = document.getElementById('OptionMain3-header3');

const sousOption1Header3 = document.getElementById('sous-option1-header3');
const sousOption2Header3 = document.getElementById('sous-option2-header3');
const sousOption3Header3 = document.getElementById('sous-option3-header3');

const BigOption1Header3 = document.getElementById('Big-Option1-Header3');
const BigOption2Header3 = document.getElementById('Big-Option2-Header3');
const BigOption3Header3 = document.getElementById('Big-Option3-Header3');

const burgerHeader3 = document.getElementById('burger-header3')
let AllOptionHeader3Switch = [false, false, false]
let header3Switch = false;

burgerHeader3.addEventListener('click', () => {
    if (!header3Switch) {
        BigOption1Header3.style.display = 'flex';
        BigOption2Header3.style.display = 'flex';
        BigOption3Header3.style.display = 'flex';
        header3Switch = true;
    } else {
        BigOption1Header3.style.display = 'none';
        BigOption2Header3.style.display = 'none';
        BigOption3Header3.style.display = 'none';
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        header3Switch = false;
        AllOptionHeader3Switch = [false, false, false]
    }
})



MainOptions1Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[0] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'flex';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [true, false, false]
    }
})

MainOptions2Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[1] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'flex';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, true, false]
    }
})

MainOptions3Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[2] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'flex';
        AllOptionHeader3Switch = [false, false, true]
    }
})


BigOption1Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[0] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'flex';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [true, false, false]
    }
})

BigOption2Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[1] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'flex';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, true, false]
    }
})

BigOption3Header3.addEventListener('click', () => {
    if (AllOptionHeader3Switch[2] === true) {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'none';
        AllOptionHeader3Switch = [false, false, false]
    } else {
        sousOption1Header3.style.display = 'none';
        sousOption2Header3.style.display = 'none';
        sousOption3Header3.style.display = 'flex';
        AllOptionHeader3Switch = [false, false, true]
    }
})